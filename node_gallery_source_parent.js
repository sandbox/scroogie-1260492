(function ($) {
  Drupal.behaviors.nodeGallerySource = function() {
    var self = this;
    self.fid_id = '';
    
    $('.node-gallery-source-link:not(.node-gallery-source-link-processed)').addClass('node-gallery-source-link-processed').click(function() {
      function onSubmitCallbackExample() {
        if ($('#'+self.fid_id).val() != '') {
          $('#'+self.fid_id).trigger('change');
        }
      }

      self.fid_id = $(this).attr('rel');
      if (self.fid_id == '') {
        alert("ERROR");
      }
      var modalOptions = {
        url: $(this).attr('href'),
        autoFit: false,
        onSubmit: onSubmitCallbackExample,
        width: 760,
        height: 560
      };
      Drupal.modalFrame.open(modalOptions);
      return false;
    });
  };
})(jQuery);
