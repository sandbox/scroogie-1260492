(function ($) {
  Drupal.behaviors.nodeGallerySource = function() {
    var self = this;
    var context = $('div.view-node-gallery-source');
    self.fid_id = Drupal.settings.node_gallery_source.fid_id;
    
    // Add a click event to all field content that searches all fields for the span that we add with the image.
    // A bit complicated, but this should work if the user adds additional fields.
    $('span.field-content', context).children().click(function() {
      fid = parseInt($(this).parents('td').find('span.node-gallery-source-identifier').attr('id'));      
      parent.jQuery('#'+self.fid_id).val(fid);
      Drupal.modalFrameChild.triggerParentEvent('childClose', false);
      return false;
    });
  };
})(jQuery);
